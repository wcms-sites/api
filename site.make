core = 7.x
api = 2

; uw_api_registration
projects[uw_api_registration][type] = "module"
projects[uw_api_registration][download][type] = "git"
projects[uw_api_registration][download][url] = "https://git.uwaterloo.ca/wcms/uw_api_registration.git"
projects[uw_api_registration][download][tag] = "7.x-1.1"
